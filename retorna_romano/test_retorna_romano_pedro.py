import unittest
import retorna_romano_pedro


class TestRetorna_romano(unittest.TestCase):

    def test_numero_igual_0(self):
        print("Teste numero 0: ", end="")
        numero = 0
        teste = retorna_romano_pedro.valida_numero( numero )
        self.assertFalse(teste)

    def test_numero_igual_3999(self):
        print("Teste numero 3999: ", end="")
        numero = 3999
        teste = retorna_romano_pedro.valida_numero( numero )
        self.assertFalse(teste)

    def test_numero_igual_4000(self):
        print("Teste numero 4000: ", end="")
        numero = 4000
        teste = retorna_romano_pedro.valida_numero( numero )
        self.assertFalse(teste)

    def test_numero_negativo(self):
        print("Teste numero negativo: ", end="")
        numero = -1
        teste = retorna_romano_pedro.valida_numero( numero )
        self.assertFalse(teste)

    def test_numero_valido(self):
        print("Teste numero válido: ", end="")
        numero = 3500
        teste = retorna_romano_pedro.valida_numero( numero )
        self.assertTrue(teste)

    def test_string(self):
        print("Teste com string: ", end="")
        numero = "string"
        teste = retorna_romano_pedro.valida_numero( numero )
        self.assertFalse(teste)

    def test_converte_20(self):
        print("Teste conversao numero 20: ", end="")
        numero = 20
        numero = retorna_romano_pedro.valida_numero( numero )
        teste = retorna_romano_pedro.convercao_romana( numero )
        self.assertEqual(teste, "XX")

    def test_converte_3528(self):
        print("Teste conversao numero 3528: ", end="")
        numero = 3528
        numero = retorna_romano_pedro.valida_numero(numero)
        teste = retorna_romano_pedro.convercao_romana(numero)
        self.assertEqual(teste, "MMMDXXVIII")


if __name__ == '__main__':
    unittest.main()
