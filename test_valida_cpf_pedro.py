from unittest import TestCase
import validacpf_pedro


class TestValida_cpf(TestCase):

    def testa_cpf_valido(self):
        cpf = "42770967860"
        validacpf_pedro.valida_cpf(cpf)
        self.assertEqual(True, validacpf_pedro.valida_cpf(cpf))

    def testa_cpf_valido_com_caracteres_especiais(self):
        cpf = "2o3i4-5o6o7-8ç9ç0/2p2p"
        cpf = validacpf_pedro.retira_formatacao(cpf)
        self.assertEqual("23456789022", cpf)

    def testa_cpf_valido_com_caracteres_especiais_e_cpf_valido(self):
        cpf = "340.809.990-11"
        cpf_valido_com_caracteres_especiais = validacpf_pedro.valida_cpf(validacpf_pedro.retira_formatacao(cpf))
        self.assertEqual(True, cpf_valido_com_caracteres_especiais)

    def testa_cpf_invalido_com_numeros_repetidos(self):
        cpf1 = "11111111111"
        cpf2 = "99999999999"
        cpf1 = validacpf_pedro.valida_cpf(cpf1)
        cpf2 = validacpf_pedro.valida_cpf(cpf2)
        self.assertEqual(False, cpf1)
        self.assertEqual(False, cpf2)

    def testa_cpf_com_numeros_inferiores_a_11(self):
        cpf = "427709678"      # len 10
        cpf2 = "42770967860"   # len 11 True
        cpf3 = "427709678602"  # len 12
        cpf1 = validacpf_pedro.valida_cpf(cpf)
        cpf2 = validacpf_pedro.valida_cpf(cpf2)
        cpf3 = validacpf_pedro.valida_cpf(cpf3)
        self.assertEqual(False, cpf1)
        self.assertEqual(True, cpf2)
        self.assertEqual(False, cpf3)





